// IUserService.aidl
package com.example.server;

// Declare any non-default types here with import statements

interface IUserService {
    String getUserName();
}