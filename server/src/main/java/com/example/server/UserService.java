package com.example.server;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


/**
 * @author user
 */
public class UserService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    IBinder binder = new IUserService.Stub() {
        @Override
        public String getUserName() {
            return "法外狂徒-张三";
        }
    };
}
